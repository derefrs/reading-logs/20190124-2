# 20190124 - Deref.rs \#2

Thu, 24. Jan. 2019

https://derefrs.connpass.com/event/116718/

```toml
[meetup]
name = "Deref Rust"
date = "20190124"
version = "0.0.2"
attendees = [
  "Yasuhiro Asaka <yasuhiro.asaka@grauwoelfchen.net>"
]
repository = "https://gitlab.com/derefrs/reading-logs/20190124-2"
keywords = ["Rust"]

[locations]
park6 = {site = "Roppongi"}
zulip = {site = "https://derefrs.zulipchat.com", optional = true }
```

## Notes

| Name | Snippet / Note / What I did in few words |
|--|--|
| @grauwoelfchen | [Read 1 blog post and a release note about Rust 1.32.0](https://gitlab.com/derefrs/reading-logs/20190124-2/snippets/1800650) |


## Links

* [Deref Rust - GitLab.com](https://gitlab.com/derefrs)
* [Deref Rust - Zulip Chat](https://derefrs.zulipchat.com/)

## License

`MIT`

```text
Reading Logs
Copyright (c) 2019 Deref.rs

This is free software: You can redistribute it and/or modify
it under the terms of the MIT License.
```
